# Prettier Pipeline

[![pipeline status](https://gitlab.com/brettops/pipelines/prettier/badges/main/pipeline.svg)](https://gitlab.com/brettops/pipelines/prettier/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Format things with [Prettier](https://prettier.io/).

## Usage

Include the pipeline:

```yaml
include:
  - project: brettops/pipelines/prettier
    file: include.yml
```
